# Bitbucket Pipelines Pipe: AWS ECS deploy many

Deploy a containerized application to AWS ECS as a new ALB loadbalanced service, using the version/tag prefixed to the domain (essentially a subdomain) as a routing key. 

Assuming you have:

- an ALB hosting a site at example.com using a wildcard dns name e.g. *.example.com
- SERVICE_NAME_PREFIX=foo
- VERSION=0.1.0

Assuming the above, that you have an ALB hosted under *.example.com, with listener rules routing existing traffic to existing services, this script will add a new service named foo-0-1-1 and add a high priority listener rule which inspects the host header and routes foo-0-1-1.example.com to the new service. 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: aaronhs/aws-ecs-deploy-many:0.1.0
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    CLUSTER_NAME: '<string>'
    SERVICE_NAME_PREFIX: '<string>'
    TASK_DEFINITION: '<string>'
    # WAIT: '<boolean>' # Optional 
    # DEBUG: '<string>' # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)              | AWS key id. |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key. |
| AWS_DEFAULT_REGION (**) | AWS region. |
| CLUSTER_NAME (*) | The name of your ECS cluster. |
| SERVICE_NAME_PREFIX (*) | The name of your ECS service. |
| TASK_DEFINITION (*) | Path to the task definition json file. The file should contain a task definition as described in the [AWS docs](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html) |
| WAIT | Wait until the service becomes stable. Default: `false`. The execution will fail if the service doensn't become stable and the timeout of 10 minutes is reached. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Prerequisites

To use this pipe you should have an ECS cluster running a service. Learn how to create one [here](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_GetStarted_EC2.html).

## Examples

### Basic example:

```yaml
script:
  - pipe: aaronhs/aws-ecs-deploy-many:0.1.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      CLUSTER_NAME: 'my-ecs-cluster'
      SERVICE_NAME_PREFIX: 'my-ecs-service'
      TASK_DEFINITION: 'task-definition.json'
```

Basic example. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: aaronhs/aws-ecs-deploy-many:0.1.0
    variables:
      CLUSTER_NAME: 'my-ecs-cluster'
      SERVICE_NAME_PREFIX: 'my-ecs-service'
      TASK_DEFINITION: 'task-definition.json'
```
